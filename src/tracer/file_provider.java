package tracer;

import java.io.*;
import java.net.InetAddress;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class file_provider {

    public void createFile(String location) {

        try {

            //Convert location to file
            File file_location = new File(location);

            //Creates the file
            file_location.createNewFile();

        } catch(IOException e) {
            e.printStackTrace();
        }

    }

    public void writeToFile(String location, String content) {

        try {

            //Convert location to file
            File file_location = new File(location);

            //Write default config content to default content
            FileWriter fw = new FileWriter(file_location.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(content);
            bw.close();

        } catch(IOException e) {
            e.printStackTrace();
        }

    }
    
    public void addToLogFile(String event) {
        //Get everything from log
        String stream = "";
        String stream_array[] = readFromFile("/var/log/tracer.log", false);
        
        for (int i = 0; i < getConfigLenth("/var/log/tracer.log"); i++) {
            stream += stream_array[i] + "\n";
        }
        
        //Add to log
        stream += gimme_time() + event;
        
        //Write log
        writeToFile("/var/log/tracer.log", stream);
    }
    
    public void report_secbre(String event) {
        //Get everything from log
        String stream = "";
        String stream_array[] = readFromFile("/var/log/tracer-secbre.log", false);
        
        for (int i = 0; i < getConfigLenth("/var/log/tracer-secbre.log"); i++) {
            stream += stream_array[i] + "\n";
        }
        
        //Add to log
        stream += gimme_time() + event;
        
        //Write log
        writeToFile("/var/log/tracer-secbre.log", stream);
        
        addToLogFile(event);
    }

    public int getConfigLenth(String location) {

        int config_file_lenth = -1;

        try {

            //Setup FileReader
            FileReader fr = new FileReader(location);
            BufferedReader br = new BufferedReader(fr);

            String re = "";
            while (re != null) {
                re = br.readLine();
                config_file_lenth++;
            }

            //Close FileReader
            br.close();

        } catch (Exception e) {
        }

        return config_file_lenth;
    }

    public String[] readFromFile(String location, boolean ignoreComments) {

        String config_file[] = new String[getConfigLenth(location)];

        try {

            //Setup FileReader
            FileReader fr0 = new FileReader(location);
            BufferedReader br0 = new BufferedReader(fr0);

            //Save whole config to a config array
            for (int i = 0; i < getConfigLenth(location); i++) {
                config_file[i] = br0.readLine();
            }

            //Close FileReader
            br0.close();

        } catch (Exception e) {
        }
        
        if (ignoreComments) {
            
            // Get the ConfigFile Lenth without #'s
            int config_file_lenth = 0;
            for (int i = 0; i < config_file.length; i++) {
                if (!config_file[i].startsWith("#")) {
                    config_file_lenth++;
                }
            }

            // Set all values of config_file without # lines to config_file_fixed
            String config_file_fixed[] = new String[config_file_lenth];
            int i2 = 0;
            for (int i = 0; i < config_file.length; i++) {
                if (!config_file[i].startsWith("#")) {
                    config_file_fixed[i2] = config_file[i];
                    i2++;
                }
            }
            
            // Reset config_file
            config_file = null;
            
            // Set config_file_fixed as config_file
            config_file = config_file_fixed;
 
        }

        //Return config array
        return config_file;

    }
    
    public void copyFile(String source_raw, String dest_raw) {
        
        File source = new File(source_raw);
        File dest = new File(dest_raw);
        
        try {
            
            Files.copy(source.toPath(), dest.toPath());
            Thread.sleep(500);
            
        } catch (Exception e) {
            //e.printStackTrace();
        }
        
         
    }

    public String parseValue(String value) {

        //Get parseable config_file array
        String config_file[] = readFromFile("/etc/tracer.conf", false);

        // Vars for getting extracted content
        int position_a = 0;
        int position_b = 0;
        String extract = "VALUE NOT FOUND";

        //Search for value
        for (int i = 0; i < getConfigLenth("/etc/tracer.conf") ; i++) {
            // Search the Line with the correct value
            if (config_file[i].startsWith(value)) {
                //Found correct Line; -> Parse
                extract = ""; //Reset the Issue Msg

                //Find the Positions
                for (int u = 0; u < config_file[i].length() ; u++) {
                    char current_position = config_file[i].charAt(u);
                    if (current_position == ':') {
                        position_a = u;
                    }
                    if (current_position == ';') {
                        position_b = u;
                    }
                }

                //Get the extract
                for (int u = position_a + 1; u < position_b ; u++) {
                    char current_position = config_file[i].charAt(u);
                    extract += current_position;
                }

            }
        }

        //Return content of value
        return extract;

    }

    public void initial_setup() {
        // Default Config File location
        File default_config = new File("/etc/tracer.conf");
        
        //  Old Postfix Config File location
        File old_postfix_config = new File("/etc/postfix/main.cf");

        //Check if File exists
        if (!default_config.exists()) {

            //Creates the file
            createFile("/etc/tracer.conf");
            createFile("/etc/iplist.conf");
            createFile("/etc/hostlist.conf");
            createFile("/etc/maclist.conf");
            createFile("/etc/powerprotected.conf");
            createFile("/etc/emergerun.sh");
            createFile("/var/log/tracer.log");
            createFile("/var/log/tracer-secbre.log");

            //Default config does not exist! Create it then exit
            System.out.println("[WARN] Config does not exist!");

            //Define default config content
            String content0 = "# This is the configuration file from " + Tracer.version
                    + "\n# Modes: blacklist , whitelist \n"
                    + "mode:blacklist;\n"
                    + "# IP Adresses for your mode (one per line!)\n"
                    + "ips:/etc/iplist.conf;\n"
                    + "# Host Names for your mode (one per line!)\n"
                    + "hosts:/etc/hostlist.conf;\n"
                    + "# MAC Adresses: \n"
                    + "macs:/etc/maclist.conf;\n"
                    + "# Power Protected Mashine list: \n"
                    + "powpro:/etc/powerprotected.conf;\n"
                    + "# Default ping time in ms\n"
                    + "df_ping:5000;\n"
                    + "# Default waiting time after a security breach in ms\n"
                    + "df_secbre:5000;\n"
                    + "# Default waiting time for complete scan in ms\n"
                    + "df_fst:200;\n"
                    + "# Default waiting time after a complete trace in ms\n"
                    + "df_comptrace:60000;\n"
                    + "# Default start subnet for class C scan\n"
                    + "df_ssnc:0;\n"
                    + "# Default end subnet for class C scan\n"
                    + "df_senc:255;\n"
                    + "# Custom Bash execution on SecBre ?\n"
                    + "opt_emru:false;\n"
                    + "# Custom Bash execution script\n"
                    + "emru_loc:/etc/emergerun.sh;\n"
                    + "# Send Mail on SecBre ?\n"
                    + "opt_sendmail:false;\n"
                    + "# Mail for reporting SecBre\n"
                    + "reportm:test@example.com;\n"
                    + "# Poweroff on SecBre ?\n"
                    + "opt_poof:false;\n"
                    + "# Play alert sound ? \n"
                    + "opt_alert:false;\n"
                    + "# Do Deepscan (arp ping) ? \n"
                    + "opt_arppi:false;\n"
                    + "# SecBre on arp ping detect ? \n"
                    + "opt_sboapd:false;\n"
                    + "# Be Verbose\n"
                    + "verbose:false;\n";
            
            String content1 = "# This is the default IPList from " + Tracer.version;
            String content2 = "# This is the logfile from " + Tracer.version;
            String content3 = "# This is the security breach logfile from " + Tracer.version;
            String content4 = "# This is the default Hostname List from " + Tracer.version;
            String content5 = "#! /bin/sh\n# This is the optional emerge execution script from " + Tracer.version + "\necho  \"Hello World!\" >> ~/TracerTest.log";
            String content6 = "# This is the Power Protected List from " + Tracer.version;
            String content7 = "# This is the default MAC List from " + Tracer.version;
            
            //Write default config content to default content
            writeToFile("/etc/tracer.conf", content0);
            writeToFile("/etc/iplist.conf", content1);
            writeToFile("/var/log/tracer.log", content2);
            writeToFile("/var/log/tracer-secbre.log", content3);
            writeToFile("/etc/hostlist.conf", content4);
            writeToFile("/etc/emergerun.sh", content5);
            writeToFile("/etc/powerprotected.conf", content6);
            writeToFile("/etc/maclist.conf", content7);
            
            //Backup old config if there is one
            if (old_postfix_config.exists()) {
                copyFile("/etc/postfix/main.cf", "/etc/postfix/main.cf.backup_origin");
            }
            
            //Setup Postfix
            exec("SetupPostfix.sh");

            //Log
            System.out.println("[WARN] Config created in /etc/tracer.conf\n"
                             + "[FAIL] Please configure Tracer first!");

            //Suicide
            System.exit(1);

        } else {
            //Config exists! Continue Programm
        }
    }
    
    public String gimme_time() {
        return " [" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + "] ";
    }
    
    public void exec(String file) {
        
        try {
            Process p = new ProcessBuilder("sh", file).start();
        } catch (Exception e) {
        }
        
    }
    
    public void exec(String file, String parm0) {
        
        try {
            Process p = new ProcessBuilder("sh", file, parm0).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void execPython2(String file, String parm0) {
        
        try {
            Process p = new ProcessBuilder("python2", file, parm0).start();
        } catch (Exception e) {
        }
        
    }
    
    public void exec(String file, String parm0, String parm1) {
        
        try {
            Process p = new ProcessBuilder("sh", file, parm0, parm1).start();
        } catch (Exception e) {
        }
        
    }
    
    public String getMAC(String IP_Addr) {
        //execPython2("getMacAddr.py", IP_Addr);
        exec("getMacAddr.sh", IP_Addr);
        
        try {
            Thread.sleep(500);
        } catch (Exception e) {
        }
        
        String macAddr[] = readFromFile("lastMac.log", false);
        
        try {
            Thread.sleep(500);
        } catch (Exception e) {
        }
        
        return macAddr[0];
    }
    
    public void sendMail(String context, String to) {
        
        //Start Email Server
        exec("/home/dmusiolik/test.sh");
        
        //Start Email Server
        exec("StartPostfix.sh");
        
        try {
            Thread.sleep(1500);
        } catch (Exception e) {
        }
        
        //Send Mail
        exec("SendEmail.sh", gimme_time() + context + " in " + tracer.Tracer.mode + " mode!", to);
        
        try {
            Thread.sleep(1500);
        } catch (Exception e) {
        }
        
        //Poweroff Email Server
        exec("ShutdownPostfix.sh");

    }
    
    //Check Online state
    public boolean ping(String IP_Target, int ping_time) {
    
        boolean reachable = false;
        
        try {
            reachable = InetAddress.getByName(IP_Target).isReachable(ping_time);
        } catch (Exception e) {
        }
        
        //Deepscan
        if (!reachable && Tracer.optional_do_deepscan) {
            
            if (Tracer.verbose) {
                // It seems the mashine is not reachable; But maybe it just blocks ICPM
                System.out.println("[INFO] VERBOSE -> Target " + IP_Target + " seems unreachable!");
                System.out.println("[WARN] VERBOSE -> Starting deepscan!");
            }
            
            //If MAC is not "--", the Target must be Online!
            String macAddr = getMAC(IP_Target);
            if (!macAddr.contains("--") && !macAddr.contains("w") && !macAddr.contains("e") && !macAddr.contains("p")) {
                //Target is ONLINE!
                if (Tracer.verbose) {
                    System.out.println("[WARN] VERBOSE -> TARGET " + IP_Target + " SEEMS TO BE ONLINE!");           
                    System.out.println("[WARN] VERBOSE -> MAC:" + macAddr);
                }
                //Report ?
                if (Tracer.optional_secbre_on_arp_ping_detect) {
                    //Report it
                    Tracer.report("[ALERT] The Target " + IP_Target + " MAC:" + macAddr + " could be a intruder! (Found online stats via deepscan)");
                }
                reachable = true;
            }
            
        }
        
        return reachable;
    }

}
