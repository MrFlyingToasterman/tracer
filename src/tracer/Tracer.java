package tracer;


public class Tracer {

    //Varz
    private static file_provider fp = new file_provider();
    private static misc_Provider mp = new misc_Provider();
    public static String version = "Tracer V 0.1 a";
    public static String mode = "ERROR GETTING MODE FROM CONFIG FILE!";
    public static String iplist_location;
    public static String hostlist_location;
    public static String maclist_location;
    public static String IPList[];
    public static String Hostlist[];
    public static String MAClist[];
    public static String PowerProtectedList[];
    public static int default_ping_time;
    public static int default_security_breach_time;
    public static int default_completed_trace_time;
    public static int default_full_scan_time;
    public static int default_start_subnet_class_c;
    public static int default_end_subnet_class_c;
    public static String optional_emergerun_location;
    public static String optional_powerprotectionlist_location;
    public static boolean optional_emergerun = false;
    public static String optional_report_mail;
    public static boolean optional_sendmail = false;
    public static boolean optional_poweroff = false;
    public static boolean optional_play_alert_sound = false;
    public static boolean optional_do_deepscan = false;
    public static boolean optional_secbre_on_arp_ping_detect = false;
    public static boolean verbose = false;

    public static void main(String args[]) {
        //Greeting
        System.out.println("[INFO] Welcome to " + version);

        //Run Initial Setup if needed
        fp.initial_setup();
        
        //Read Mode from config
        mode = fp.parseValue("mode");
        System.out.println("[INFO] Choosen mode: " + mode);
        
        //Get IPs
        System.out.println("[INFO] Getting IPList location ...");
        iplist_location = fp.parseValue("ips");
        System.out.println("[INFO] Getting IPs from >" + iplist_location + "<");
        IPList = fp.readFromFile(iplist_location, true);
        
        //Get Hosts
        System.out.println("[INFO] Getting Hostlist location ...");
        hostlist_location = fp.parseValue("hosts");
        System.out.println("[INFO] Getting Hosts from >" + hostlist_location + "<");
        Hostlist = fp.readFromFile(hostlist_location, true);
        
        //Get MACs
        System.out.println("[INFO] Getting Maclist location ...");
        maclist_location = fp.parseValue("macs");
        System.out.println("[INFO] Getting MACs from >" + maclist_location + "<");
        MAClist = fp.readFromFile(maclist_location, true);
        
        //Get Power Protected IPs
        System.out.println("[INFO] Getting Power Protected List location ...");
        optional_powerprotectionlist_location = fp.parseValue("powpro");
        System.out.println("[INFO] Getting Power Protected List from >" + optional_powerprotectionlist_location + "<");
        PowerProtectedList = fp.readFromFile(optional_powerprotectionlist_location, true);
        
        //Get Default things
        System.out.println("\n[INFO] Getting defaults ... \n");
        default_ping_time = Integer.parseInt(fp.parseValue("df_ping"));
        System.out.println("[INFO] df_ping is " + default_ping_time);
        default_security_breach_time = Integer.parseInt(fp.parseValue("df_secbre"));
        System.out.println("[INFO] df_secbre is " + default_security_breach_time);
        default_completed_trace_time = Integer.parseInt(fp.parseValue("df_comptrace"));
        System.out.println("[INFO] df_comptrace is " + default_completed_trace_time);
        default_full_scan_time = Integer.parseInt(fp.parseValue("df_fst"));
        System.out.println("[INFO] df_fst is " + default_full_scan_time);
        default_start_subnet_class_c = Integer.parseInt(fp.parseValue("df_ssnc"));
        System.out.println("[INFO] df_ssnc is " + default_start_subnet_class_c);
        default_end_subnet_class_c = Integer.parseInt(fp.parseValue("df_senc"));
        System.out.println("[INFO] df_senc is " + default_end_subnet_class_c);
        
        // Check if default_start_subnet_class_c ip is valid
        if (!mp.checkIP(default_start_subnet_class_c)) {
            //Invalid IP
            System.out.println("[FAIL] df_ssnc IP is Invalid!");
            System.exit(1);
        } else {
            //Valid IP
            System.out.println("[INFO] df_ssnc IP is Valid!");
        }
        
        // Check if default_end_subnet_class_c ip is valid
        if (!mp.checkIP(default_end_subnet_class_c)) {
            //Invalid IP
            System.out.println("[FAIL] df_senc IP is Invalid!");
            System.exit(1);
        } else {
            //Valid IP
            System.out.println("[INFO] df_senc IP is Valid!");
        }
        
        //Get Optinal things
        System.out.println("\n[INFO] Getting optionals ... \n");
        optional_emergerun_location = fp.parseValue("emru_loc");
        optional_report_mail = fp.parseValue("reportm");

        // Get optional_sendmail state
        if (fp.parseValue("opt_sendmail").equalsIgnoreCase("true")) {
            optional_sendmail = true;
        }
        
        // Get optional_do_deepscan state
        if (fp.parseValue("opt_arppi").equalsIgnoreCase("true")) {
            optional_do_deepscan = true;
        }
        
        // Get optional_secbre_on_arp_ping_detect state
        if (fp.parseValue("opt_sboapd").equalsIgnoreCase("true")) {
            optional_secbre_on_arp_ping_detect = true;
        }
        
        // Get optional_play_alert_sound state
        if (fp.parseValue("opt_alert").equalsIgnoreCase("true")) {
            optional_play_alert_sound = true;
        }
        
        // Get optional_emergerun state
        if (fp.parseValue("opt_emru").equalsIgnoreCase("true")) {
            optional_emergerun = true;
        }
        
        // Get optional_poweroff (on SecBre) state
        if (fp.parseValue("opt_poof").equalsIgnoreCase("true")) {
            optional_poweroff = true;
        }
        
        // Get Verbose state
        if (fp.parseValue("verbose").equalsIgnoreCase("true")) {
            verbose = true;
        }
        
        System.out.println("[INFO] verbose is " + verbose);
        
        //Show Summary
        System.out.println("\n[INFO] Create Summary..\n\n"
                + "[INFO] The following IP adresses are targets now:\n");
        for (int i = 0; i < IPList.length; i++) {
            System.out.println("[TARGET] " + IPList[i]);
        }
        System.out.println("\n[INFO] The following Hostnames are targets now:\n");
        for (int i = 0; i < Hostlist.length; i++) {
            System.out.println("[TARGET] " + Hostlist[i]);
        }
        System.out.println("\n[INFO] The following MAC Adresses are targets now:\n");
        for (int i = 0; i < MAClist.length; i++) {
            System.out.println("[TARGET] " + MAClist[i]);
        }
        System.out.print("\n");
        
        //Log start
        fp.addToLogFile("Starting Tracer in " + mode + " mode!\n");
        
        //Copy shell files
        fp.copyFile("../shell/SendEmail.sh", "SendEmail.sh");
        fp.copyFile("../shell/SetupPostfix.sh", "SetupPostfix.sh");
        fp.copyFile("../shell/ShutdownPostfix.sh", "ShutdownPostfix.sh");
        fp.copyFile("../shell/StartPostfix.sh", "StartPostfix.sh");
        fp.copyFile("../shell/poweroff.sh", "poweroff.sh");
        fp.copyFile("../shell/ppKill.sh", "ppKill.sh");
        fp.copyFile("../shell/getMacAddr.sh", "getMacAddr.sh");
        
        //Start Trace
        mainloop(mode);
    }
    
    private static void mainloop(String mode) {
        
        //Loop Switch
        boolean repeter = true;
        
        switch(mode) {    
            case "blacklist":
                //Main loop for blacklist; A Mashine is not allowed to be online
                do {            
                    System.out.println("[INFO] Started Trace");
                    //Check if Targeted Mashine is reachable
                    for (int i = 0; i < IPList.length; i++) {
                        boolean reachable = false;

                        //Ping
                        reachable = fp.ping(IPList[i], default_ping_time);

                        if (reachable == true) {
                            // security breach!
                            System.err.println("[WARN] Security breach! from target: " + mp.getHostname(IPList[i]) + "@" + IPList[i] + " MAC:" + fp.getMAC(IPList[i]));
                            
                            //Report
                            report("[WARN] Security breach! from target: " + mp.getHostname(IPList[i]) + "@" + IPList[i] + " MAC:" + fp.getMAC(IPList[i]));
                            
                            //Wait
                            System.out.println("[INFO] Waiting " + default_security_breach_time + "ms.");
                            try {
                                Thread.sleep(default_security_breach_time);
                            } catch (Exception e) {
                            } 
                            System.out.println("[INFO] Trace continues!");
                        }                           
                        
                    }
                    
                    //Complete Scan; Check for Hostnames and Mac Adresses
                    System.out.println("[INFO] Starting full trace!");
                    for(int x = default_start_subnet_class_c; x <= default_end_subnet_class_c; x++) {
                        for (int y = 1; y < 255; y++) {
                            boolean reachable = false;
                            
                            String current_ip = "192.168." + x + "." + y;
                            
                            if (verbose) {
                                System.out.println("[INFO] VERBOSE -> Current IP: " + current_ip);
                            }

                            //Ping
                            reachable = fp.ping(current_ip, default_full_scan_time);
                            
                            if (reachable) {
                                if (verbose) {
                                System.out.println("[INFO] VERBOSE -> " + mp.getHostname(current_ip) + "@" + current_ip + " is online!");
                                System.out.println("[INFO] VERBOSE -> " + current_ip + " got MAC:" + fp.getMAC(current_ip));
                                fp.addToLogFile("[INFO] VERBOSE -> " + current_ip + " is online!");
                                fp.addToLogFile("[INFO] VERBOSE -> " + current_ip + " got Hostname: " + mp.getHostname(current_ip));
                                fp.addToLogFile("[INFO] VERBOSE -> " + current_ip + " got MAC:" + fp.getMAC(current_ip));
                                }
                                
                                //Check Hostname
                                for (int i = 0; i < Hostlist.length; i++) {
                                    if (mp.getHostname(current_ip).equals(Hostlist[i])) {
                                        // security breach!
                                        System.err.println("[WARN] Security breach! from target: " + mp.getHostname(current_ip) + "@" + current_ip + " MAC:" + fp.getMAC(current_ip));

                                        //Report
                                        report("[WARN] Security breach! from target: " + mp.getHostname(current_ip) + "@" + current_ip + " MAC:" + fp.getMAC(current_ip));

                                        //Wait
                                        System.out.println("[INFO] Waiting " + default_security_breach_time + "ms.");
                                        try {
                                            Thread.sleep(default_security_breach_time);
                                        } catch (Exception e) {
                                        } 
                                        System.out.println("[INFO] Trace continues!");
                                    }
                                }
                                
                                //Check MAC Adresses
                                for (int i = 0; i < MAClist.length; i++) {
                                    if (fp.getMAC(current_ip).equals(MAClist[i])) {
                                        // security breach!
                                        System.err.println("[WARN] Security breach! from target: " + mp.getHostname(current_ip) + "@" + current_ip + " MAC:" + fp.getMAC(current_ip));

                                        //Report
                                        report("[WARN] Security breach! from target: " + mp.getHostname(current_ip) + "@" + current_ip + " MAC:" + fp.getMAC(current_ip));

                                        //Wait
                                        System.out.println("[INFO] Waiting " + default_security_breach_time + "ms.");
                                        try {
                                            Thread.sleep(default_security_breach_time);
                                        } catch (Exception e) {
                                        } 
                                        System.out.println("[INFO] Trace continues!");
                                    }
                                }
                            }
                        }
                    }
                    
                    //Wait after complete trace
                    try {
                        Thread.sleep(default_completed_trace_time);
                    } catch (Exception e) {
                    } 
                } while (repeter);
                break;
                case "whitelist":
                    //Main loop for whitelist; Not targeted mashines are not allowed to be online!
                    do {            
                        
                        //Complete Scan; Check for Hostnames and Mac Adresses
                        System.out.println("[INFO] Starting full trace!");
                        for(int x = default_start_subnet_class_c; x <= default_end_subnet_class_c; x++) {
                            for (int y = 1; y < 255; y++) {
                                boolean reachable = false;

                                String current_ip = "192.168." + x + "." + y;

                                if (verbose) {
                                    System.out.println("[INFO] VERBOSE -> Current IP: " + current_ip);
                                }

                                //Ping
                                reachable = fp.ping(current_ip, default_full_scan_time);

                                if (reachable) {
                                    if (verbose) {
                                    System.out.println("[INFO] VERBOSE -> " + mp.getHostname(current_ip) + "@" + current_ip + " is online!");
                                    System.out.println("[INFO] VERBOSE -> " + current_ip + " got MAC:" + fp.getMAC(current_ip));
                                    fp.addToLogFile("[INFO] VERBOSE -> " + current_ip + " is online!");
                                    fp.addToLogFile("[INFO] VERBOSE -> " + current_ip + " got Hostname: " + mp.getHostname(current_ip));
                                    fp.addToLogFile("[INFO] VERBOSE -> " + current_ip + " got MAC:" + fp.getMAC(current_ip));
                                    }
                                    
                                    //Check IP
                                    boolean ip_is_in_whitelist = false;
                                    for (int n = 0; n < IPList.length; n++) {
                                        if (current_ip.equals(IPList[n])) {
                                            //IP is in whitelist -> All good
                                            ip_is_in_whitelist = true;
                                        }
                                    }
                                    

                                    //Check Hostname
                                    boolean hostname_is_in_whitelist = false;
                                    for (int i = 0; i < Hostlist.length; i++) {
                                        if (mp.getHostname(current_ip).equals(Hostlist[i])) {
                                            //Hostname found in Hostlist
                                            hostname_is_in_whitelist = true;
                                        }
                                    }
                                    

                                    //Check MAC Adresses
                                    boolean mac_is_in_whitelist = false;
                                    for (int i = 0; i < MAClist.length; i++) {
                                        if (fp.getMAC(current_ip).equals(MAClist[i])) {
                                            //MAC found in Maclist
                                            mac_is_in_whitelist = true;
                                        }
                                    }
                                    
                                    //Check if everything was okey
                                    if (!ip_is_in_whitelist && !hostname_is_in_whitelist && !mac_is_in_whitelist) {
                                        //Hostname is not in whitelist
                                        // security breach!
                                        System.err.println("[WARN] Security breach! from target: " + mp.getHostname(current_ip) + "@" + current_ip + " MAC:" + fp.getMAC(current_ip));

                                        //Report
                                        report("[WARN] Security breach! from target: " + mp.getHostname(current_ip) + "@" + current_ip + " MAC:" + fp.getMAC(current_ip));

                                        //Wait
                                        System.out.println("[INFO] Waiting " + default_security_breach_time + "ms.");
                                        try {
                                            Thread.sleep(default_security_breach_time);
                                        } catch (Exception e) {
                                        } 
                                        System.out.println("[INFO] Trace continues!");
                                    }
                                }
                            }
                        }

                        //Wait after complete trace
                        try {
                            Thread.sleep(default_completed_trace_time);
                        } catch (Exception e) {
                        } 
                    } while (repeter);
                break;
            default:
                System.err.println("[FAIL] Mode >" + mode + "< cant be found!");
                System.exit(1);
                break;
        
        }
        
    }
    
    
    public static void report(String information) {
        //Reporting the security breach!
        fp.report_secbre(information);
        
        //Play Alert Sound!
        if (optional_play_alert_sound) {
            fp.exec("alert.sh");
        }
        
        //Run emergerun.sh
        if (optional_emergerun) {
            fp.exec(optional_emergerun_location);
        }
        
        //Send Email to Operator!
        if (optional_sendmail) {
            fp.sendMail(information, optional_report_mail);
        }
        
        //Poweroff remote Devices (PowerProtection)
        for (int x = 0; x < PowerProtectedList.length; x++) {
            fp.exec("ppKill.sh", PowerProtectedList[x]);
        }
        
        //Poweroff on SecBre
        if (optional_poweroff) {
            fp.exec("poweroff");
        }
    }
    
}