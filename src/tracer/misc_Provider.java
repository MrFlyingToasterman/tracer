package tracer;

import java.net.InetAddress;

public class misc_Provider {  
    
    public boolean checkIP(int IP_Adress) {
        if(IP_Adress >= 0 && IP_Adress <= 255) {
            //Valid
            return true;
        }
        //Invalid
        return false;
    }
    
    public String getHostname(String IP_Adress) {
        
        InetAddress addr = null;
        
        try {
            addr = InetAddress.getByName(IP_Adress);
        } catch (Exception e) {
        }
        
        
        String host = addr.getHostName();
        
        return host;
    }
    
}
