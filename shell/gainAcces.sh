#! /bin/sh

# This is a script to gain access on choosen SSH mashines

if [ "$2" = "" ]; then
  echo "Use: sh gainAccess user host"
  exit
fi

ssh-keygen | echo ""
ssh-copy-id $1@$2

sudo echo "$1@$2" >> /etc/powerprotected.conf

echo "Ready!"
