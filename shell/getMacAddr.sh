#! /bin/bash

#Check if Parameters are set
if [ "$1" = "" ]; then
  echo "Use: getMacAddr <TARGET-IP>"
  exit
fi

#Get Mac Addr
MAC=$(arp -n $1 | awk '/'$1'/{print $3;exit}')

#Check if MAC is empty
if [ "$MAC" = "" ]; then
	#MAC is empty
	echo "--" > lastMac.log
	exit
fi

#Mac is not empty
echo $MAC > lastMac.log
