#! /bin/sh

if ["$1" = ""]; then
	echo "Use: $ sudo addToMacFilter.sh <MAC-ADDR> <DROP/REJECT>"
	exit
fi

iptables -A INPUT -m mac --mac-source $1 -j $2
