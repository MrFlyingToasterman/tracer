#! /bin/sh

echo "myhostname = mail.tracer.com
mydomain = tracer.com
myorigin = tracer.com
mydestination = mail.tracer.com, localhost.tracer.com, tracer.com, mail.tracer.com, www.tracer.com
mail_spool_directory = /var/spool/mail
inet_protocols = ipv4" > /etc/postfix/main.cf
