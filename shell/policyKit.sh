#! /bin/sh

# Check if policyKit is used correct!
if [ "$1" = "" ]; then
	echo "Use: # policyKit <create/install> <Name of Policy>"
	exit
fi

# Create a Policy
if [ "$1" = "create" ]; then
	echo "Creating a Policy from current setup..."
	mkdir $2
	cp /etc/tracer.conf $2
	cp /etc/iplist.conf $2
	cp /etc/hostlist.conf $2
	cp /etc/maclist.conf $2
	cp /etc/powerprotected.conf $2
	cp /etc/emergerun.sh $2
	tar -zcvf "POLICY-$2.tar.gz" $2
	rm -rf $2
fi

# Install a Policy
if [ "$1" = "install" ]; then
	echo "Installing Policy $2"
	mkdir new_policy
	tar -C new_policy -xzvf $2 --strip-components 1
	rm /etc/tracer.conf
	cp new_policy/tracer.conf /etc/tracer.conf
	rm /etc/iplist.conf
	cp new_policy/iplist.conf /etc/iplist.conf
	rm /etc/hostlist.conf
	cp new_policy/hostlist.conf /etc/hostlist.conf
	rm /etc/maclist.conf
	cp new_policy/maclist.conf /etc/maclist.conf
	rm /etc/powerprotected.conf
	cp new_policy/powerprotected.conf /etc/powerprotected.conf
	rm /etc/emergerun.sh
	cp new_policy/emergerun.sh /etc/emergerun.sh
	rm -rf new_policy/
fi
