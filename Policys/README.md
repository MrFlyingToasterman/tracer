# Policy Kit
Its possible to backup your current Tracer configurations via: <br>
`$ sudo sh policyKit.sh create My_Policy` <br>
<br>
This will create a `POLICY-My_Policy.tar.gz`
<br><br><br>
You can install a Policy via: <br>
`$ sudo sh policyKit.sh install POLICY-My_Policy.tar.gz` <br>
<br>
Attention! This will replace all configurations with the new ones!
