# Tracer
A small network service to check if unwanted machines are online. <br>
Designed to run on a GNU / Linux router with SSH access. <br>
Currently supports class C networks. <br>
Class A and B are also supported but not as good as a class C <br>

# Detect
Tracer permanently checks the network for the condition of the machines. <br>

# How to run
Just execute the tracer.jar on your Mashine. <br>
`$ sudo java -jar dist/tracer.jar` <br>
Now a warning appears and the program ends. <br>
It is time to look at the `/etc/tracer.conf` and configure the program. <br>
If the program is now restarted, the configurations will work. <br>
<br>
Dont forget the probably most important config `/etc/maclist.conf` <br>
Dont forget to add IPs to `/etc/iplist.conf` <br>
Dont forget to add Hostnames to `/etc/hostlist.conf` <br>

Add targets in `/etc/powerprotected.conf` via `$ sh shell/gainAccess.sh <user> <Host/IP>`

# Dependencies
Needed
- Java 1.8.0 (Base)

Optional (as long as you dont use the feature, you dont need it)
- Postfix (Send Email)
- Espeak (Alert Sound)

# Features
- Block IPs
  - Its possible to assign an IP address as a security offense and immediately report the online state to the netadmin!
  - Its possible to directly drop every package from this IP. (Via IPTables2 rule) (soon)
  
- Block Hostnames
  - Its possible to scan a whole class C network for Hostnames.
  - Its possible to assign an Hostname as a security offense and immediately report the online state to the netadmin!
  - Its possible to directly drop every package from the Hostnames IP. (Via IPTables2 rule) (soon)
  
- Block MAC Adresses
  - Its possible to scan a whole class C network for MACs.
  - Its possible to assign a MAC Address as a security offense and immediately report the online state to the netadmin!
  - Its possible to directly drop every package from the Hostnames IP. (Via IPTables2 rule) (soon)

- Block Ports (soon)
  - On a special IP
  - On a special Hostname
  - On a special MAC
  
- Deepscan
  - If a normal ping request fails, an arp ping can be sent in deep scan mode.
  - A machine that blocks ICMP can trigger a security breach and can be reported as an intruder.
  
- Power Protection
  - Poweroff sensible mashines on SecBre
  - Tracer can safely shutdown the remote machines. 
  - Via SSH Public Key.
  
- You can set a time for which machine to run. (soon)

- Blacklist Support
  - Every MAC, Hostname and IP in /etc/maclist.conf & /etc/hostlist.conf & /etc/iplist.conf will be Targeted as not allowed to be online!
  - Everything else is allowed!

- Whitelist Support
  - Every MAC, Hostname and IP in /etc/maclist.conf & /etc/hostlist.conf & /etc/iplist.conf will be Targeted as allowed to be online!
  - Everything else is a security breach!
  - This function is unfortunately only optimized for class c networks.

- Agressiv Mode (soon)
  - Its possible to switch off the router immediately in case of a security breach!
  - Its possible to attack the IP address that caused the security breach!

- In the event of a security breach, the incident is reported to the network administrator!
  - It will get listed with timestamp in a log file
  - Its possible to run a emerge shell script
  - Its possible to get a E-Mail
  - Its possible to poweroff host Mashine
  - Its possible to poweroff remote Mashine via SSH
  - Its possible to play a alert sound on SecBre
  - Its possible to get a Telegram msg (soon)
  
# Configuration
Main config: `/etc/tracer.conf` <br>

    # This is the configuration file from Tracer V 0.1 a
    # Modes: blacklist , whitelist 
    mode:blacklist;
    # IP Adresses for your mode (one per line!)
    ips:/etc/iplist.conf;
    # Host Names for your mode (one per line!)
    hosts:/etc/hostlist.conf;
    # MAC Adresses: 
    macs:/etc/maclist.conf;
    # Power Protected Mashine list: 
    powpro:/etc/powerprotected.conf;
    # Default ping time in ms
    df_ping:5000;
    # Default waiting time after a security breach in ms
    df_secbre:5000;
    # Default waiting time for complete scan in ms
    df_fst:200;
    # Default waiting time after a complete trace in ms
    df_comptrace:60000;
    # Default start subnet for class C scan
    df_ssnc:0;
    # Default end subnet for class C scan
    df_senc:255;
    # Custom Bash execution on SecBre ?
    opt_emru:false;
    # Custom Bash execution script
    emru_loc:/etc/emergerun.sh;
    # Send Mail on SecBre ?
    opt_sendmail:false;
    # Mail for reporting SecBre
    reportm:test@example.com;
    # Poweroff on SecBre ?
    opt_poof:false;
    # Play alert sound ? 
    opt_alert:false;
    # Do Deepscan (arp ping) ? 
    opt_arppi:true;
    # SecBre on arp ping detect ? 
    opt_sboapd:false;
    # Be Verbose
    verbose:true;



IP list config: `/etc/iplist.conf` <br>
    
    # This is the default IPList from Tracer V 0.1 a
    # Example file!
    192.168.2.1
    192.168.2.5
    192.168.2.9
    192.168.2.102

Hostname list config: `/etc/hostlist.conf` <br>

    # This is the default Hostname List from Tracer V 0.1 a
    # Example file!
    PC-1234
    PC-4321
    Dell-XPS
    BSD-TestVM
    
MAC list config: `/etc/maclist.conf` <br>

    # This is the default MAC List from Tracer V 0.1 a
    # Example file!
    xx:xx:xx:xx:xx:xx
    xx:xx:xx:xx:xx:xx
    xx:xx:xx:xx:xx:xx

Power Protected list config: `/etc/powerprotected.conf ` <br>

    # This is the Power Protected List from Tracer V 0.1 a
    # Example file!
    root@192.168.2.204
    root@BSD-TestVM
> Please remember to not add machines here manually! <br>
> Do it this way: `$ sh shell/gainAccess.sh <user> <Host/IP>`

Emergerun script: `/etc/emergerun.sh` <br>

    # This is the optional emerge execution script from Tracer V 0.1 a
    echo  "Hello World!" >> ~/TracerTest.log

# Policy Kit
Its possible to backup your current Tracer configurations via: <br>
`$ sudo sh policyKit.sh create My_Policy` <br>
<br>
This will create a `POLICY-My_Policy.tar.gz`
<br><br><br>
You can install a Policy via: <br>
`$ sudo sh policyKit.sh install POLICY-My_Policy.tar.gz` <br>
<br>
Attention! This will replace all configurations with the new ones!

# Log files
Main log: `/var/log/tracer.log` <br>
Security breach log: `/var/log/tracer-secbre.log` <br>

# Preview

![tracer](/preview.png?raw=true "Tracer in action!")

# License
![GNU GPLv3 Image](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
